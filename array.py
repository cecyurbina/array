#!/usr/bin/python
import sys

def get_array_moved(input_array, pos):
    output_array = input_array[len(input_array)-pos:len(input_array)] + input_array[0: len(input_array)-pos]
    return output_array

def main():
    """get positions with argv
    """
    try:
        positions = sys.argv[1]
        input_array = [1, 2, 3, 4, 5, 6]
        print "Move %s positions" %positions
        output_array = get_array_moved(input_array, int(positions))
        print output_array
    except:
        print "write the number of position"
        return

if __name__ == "__main__":
    main()
